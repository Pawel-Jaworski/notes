package pl.sda.notes;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class NoteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String content;
    private LocalDateTime createdTime;
    private LocalDateTime lastEditionTime;
    private Boolean isDeleted;

    public NoteEntity(String title, String content) {
        this.title = title;
        this.content = content;
        this.createdTime = LocalDateTime.now();
        this.lastEditionTime = LocalDateTime.now();
        this.isDeleted = false;
    }
}
