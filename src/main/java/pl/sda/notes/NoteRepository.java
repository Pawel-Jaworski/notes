package pl.sda.notes;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface NoteRepository extends JpaRepository<NoteEntity, Long> {

    List<NoteEntity> findAllByIsDeletedIsFalse();

    Optional<NoteEntity> findByIdAndIsDeletedIsFalse(Long id);

    @Query("select n from NoteEntity n where n.content like %?1% or n.title like %?1%")
    List<NoteEntity> search(String phrase);

}
