package pl.sda.notes;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class NoteDTO extends NoteMinDTO {
    private String content;
    private LocalDateTime createdTime;
    private LocalDateTime lastEditionTime;

    public NoteDTO(Long id, String title, String content, LocalDateTime createdTime, LocalDateTime lastEditionTime) {
        super(id, title);
        this.content = content;
        this.createdTime = createdTime;
        this.lastEditionTime = lastEditionTime;
    }

    @Override
    public String toString() {
        return "NoteDTO{" + "id=" + getId() + "title= " + getTitle() +
                "content='" + content + '\'' +
                ", createdTime=" + createdTime +
                ", lastEditionTime=" + lastEditionTime +
                '}';
    }
}
