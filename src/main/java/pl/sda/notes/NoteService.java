package pl.sda.notes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class NoteService {

    private final NoteRepository noteRepository;

    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public List<NoteMinDTO> getList() {
        log.info("getList");
        return noteRepository.findAllByIsDeletedIsFalse().stream()
                .map(noteEntity -> new NoteMinDTO(noteEntity.getId(), noteEntity.getTitle()))
                .collect(Collectors.toList());
    }

    public NoteDTO getOne(Long id) {
        log.info("getOne id: {}", id);
        NoteEntity noteEntity = noteRepository.findByIdAndIsDeletedIsFalse(id)
                .orElseThrow(() -> new NoteDoesNotExistException(id));
        return new NoteDTO(noteEntity.getId(),
                noteEntity.getTitle(),
                noteEntity.getContent(),
                noteEntity.getCreatedTime(),
                noteEntity.getLastEditionTime());
    }

    public void create(NoteDTO dto) {
        log.info("create");
        NoteEntity noteEntity = new NoteEntity(dto.getTitle(),
                dto.getContent());
        noteRepository.save(noteEntity);
        log.info("New note created with id: {}", noteEntity.getId());
    }

    public void update(NoteDTO dto) {
        log.info("update");
        NoteEntity noteEntity = noteRepository.findByIdAndIsDeletedIsFalse(dto.getId())
                .orElseThrow(() -> new NoteDoesNotExistException(dto.getId()));
        noteEntity.setTitle(dto.getTitle());
        noteEntity.setContent(dto.getContent());
        noteEntity.setLastEditionTime(LocalDateTime.now());
        noteRepository.save(noteEntity);
        log.info("Updated note with id: {}", noteEntity.getId());
    }

    public void delete(Long id) {
        log.info("delete note with id: {}", id);
        NoteEntity noteEntity = noteRepository.findByIdAndIsDeletedIsFalse(id)
                .orElseThrow(() -> new NoteDoesNotExistException(id));
        noteEntity.setIsDeleted(true);
        noteRepository.save(noteEntity);
        log.info("Deleted note with id: {}", id);
    }

    public List<NoteDTO> search(String phrase) {
        return noteRepository.search(phrase).stream()
                .map(noteEntity -> new NoteDTO(noteEntity.getId(), noteEntity.getTitle(), noteEntity.getContent(), noteEntity.getCreatedTime(), noteEntity.getLastEditionTime()))
                .collect(Collectors.toList());
    }
}
