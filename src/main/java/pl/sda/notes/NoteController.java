package pl.sda.notes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/notes")
public class NoteController {

    private final NoteService noteService;

    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping
    public List<NoteMinDTO> listNotes() {
        log.info("GET /notes");
        return noteService.getList();
    }

    @GetMapping("/{id}")
    public NoteDTO getOne(@PathVariable("id") Long id) {
        log.info("GET /notes for id {}", id);
        return noteService.getOne(id);
    }

    @PutMapping()
    public void create(@RequestBody NoteDTO dto) {
        log.info("PUT /notes with {}", dto);
        noteService.create(dto);
    }

    @PatchMapping()
    public void update(@RequestBody NoteDTO dto) {
        log.info("PATCH /notes with {}", dto);
        noteService.update(dto);
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable("id") Long id) {
        log.info("DELETE /notes for id {}", id);
        noteService.delete(id);
    }

    @GetMapping("/search")
    public List<NoteDTO> search(@PathParam("search") String search) {
        log.info("GET /notes/search with id: {}");
        return noteService.search(search);
    }
}
